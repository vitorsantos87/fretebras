$(document).ready(function(){

    $('#fatura_anterior').hide();
    $('#fatura_atual').show();

    $('#btn_anterior').click(function(){
        $('#fatura_anterior').show();
        $('#fatura_atual').hide();

    });

    $('#btn_atual').click(function(){
        $('#fatura_anterior').hide();
        $('#fatura_atual').show();
    });
});
