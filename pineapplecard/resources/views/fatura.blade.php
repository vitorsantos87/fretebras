@extends('layouts.principal')
@section('title', 'Home')
@section('styles')
    @parent
@stop
    @section('conteudo')
            <div class="content">
                <div class="jumbotron">
                    <h1 class="display-4">Teste Fretebras - Fatura Cliente</h1>
                    <hr class="my-4">

                    <div class="card-group">
                        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                            <div class="card-header">Limite</div>
                            <div class="card-body">
                                <h5 class="card-title">{{Transformer::moneyToView($retorno->dados->limite)}}</h5>
                            </div>
                        </div>
                      <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                        <div class="card-header">Pontos</div>
                        <div class="card-body">
                            <h5 class="card-title">{{Transformer::formatarNumeroDuasCasas($retorno->dados->saldo_pontos)}}</h5>
                        </div>
                      </div>
                    </div>

                    <hr class="my-4">
                    @php
                        $fatura_atual = $retorno->dados->fatura_atual;
                        $ultima_fatura_fechada = $retorno->dados->ultima_fatura_fechada;
                        $arr_situacao_fatura_anterior = FaturaView::getClasseTextoFaturaFechada( $ultima_fatura_fechada->fatura->data_vencimento, $ultima_fatura_fechada->fatura->status_fatura_id);
                    @endphp
                    <div class="card text-center">
                        <div class="card-header">
                          <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                            <button class="nav-link btn btn-{{$arr_situacao_fatura_anterior['classe']}}" id="btn_anterior" >Fatural Anterior</button>
                              </li>
                            <li class="nav-item">
                                <button class="nav-link btn btn-warning"  id="btn_atual"> Fatura Aberta</button>
                            </li>
                          </ul>
                        </div>

                        <div class="card-body alert alert-{{$arr_situacao_fatura_anterior['classe']}}" id="fatura_anterior" id="fatura_anterior">
                            <h5 class="card-title">Fatura Vencimento: {{ Data::DataParaView($ultima_fatura_fechada->fatura->data_vencimento) }}</h5>
                            <h5 class="card-title">Total da Fatura: {{ Transformer::moneyToView($ultima_fatura_fechada->fatura->valor_total) }}</h5>
                            <p>{{$arr_situacao_fatura_anterior['texto']}} </p>
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th scope="col">Data</th>
                                    <th scope="col">Estabelecimento</th>
                                    <th scope="col">valor</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($ultima_fatura_fechada->lancamentos as $lancamento)
                                  <tr>
                                    <td>{{Data::DataParaView($lancamento->data) . ' ' .$lancamento->hora}}</td>
                                    <td>{{$lancamento->estorno ? $lancamento->estabelecimento . ' (Estorno)' : $lancamento->estabelecimento}}</td>
                                    <td class="{{$lancamento->estorno ? 'text-danger' : ''}}">{{Transformer::moneyToView($lancamento->valor)}}</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="card-body alert alert-warning" id="fatura_atual">
                          <h5 class="card-title">Fatura Vencimento:{{ Data::DataParaView($fatura_atual->fatura->data_vencimento) }}</h5>
                          <h5 class="card-title">Total da Fatura: {{ Transformer::moneyToView($fatura_atual->fatura->valor_total) }}</h5>
                            <p> Fatura Aberta </p>
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th scope="col">Data</th>
                                    <th scope="col">Estabelecimento</th>
                                    <th scope="col">valor</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($fatura_atual->lancamentos as $lancamento)
                                  <tr>
                                    <td>{{Data::DataParaView($lancamento->data) . ' ' .$lancamento->hora}}</td>
                                    <td>{{$lancamento->estorno ? $lancamento->estabelecimento . ' (Estorno)' : $lancamento->estabelecimento}}</td>
                                    <td class="{{$lancamento->estorno ? 'text-danger' : ''}}">{{Transformer::moneyToView($lancamento->valor)}}</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>

                        </div>
                      </div>
                </div>

            </div>
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="{{ asset('js/fatura.js')}}"></script>
@stop
