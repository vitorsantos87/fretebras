<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1'], function () {
    Route::post('/acompanhamento/{cliente_id}', 'API\AcompanhamentoController@get')->name('getAcompanhamento');
});
