<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaEstabelecimentoModel extends Model
{
    protected $table = 'categorias_estabelecimentos';

    public function estabelecimentos()
    {
        return $this->hasMany('App\Models\EstabelecimentoModel', 'categoria_estabelecimento_id', 'id');
    }
}
