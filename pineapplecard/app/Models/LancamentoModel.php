<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LancamentoModel extends Model
{
    protected $table = 'lancamentos';

    public function estabelecimento()
    {
        return $this->belongsTo('App\Models\EstabelecimentoModel', 'estabelecimento_id');
    }

    public function fatura()
    {
        return $this->belongsTo('App\Models\FaturaModel','fatura_id');
    }

    public static function getLancamentosPorfatura($fatura_id){
        return self::where('fatura_id', $fatura_id)
                    ->join('estabelecimentos', 'estabelecimentos.id', '=', 'lancamentos.estabelecimento_id')
                    ->select('lancamentos.*', 'estabelecimentos.nome as estabelecimento')
                    ->orderBy('lancamentos.data', 'DESC')
                    ->orderBy('lancamentos.hora', 'DESC')
                    ->get();
    }
}
