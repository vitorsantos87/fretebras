<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusFaturaModel extends Model
{
    protected $table = 'status_faturas';

    public const ABERTA = 1;
    public const FECHADA = 2;
    public const PAGO = 3;


    public function faturas()
    {
        return $this->hasMany('App\Models\FaturaModel', 'status_fatura_id', 'id');
    }

}
