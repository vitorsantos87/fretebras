<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstabelecimentoModel extends Model
{
    protected $table = 'estabelecimentos';

    public function lancamentos()
    {
        return $this->hasMany('App\Models\LancamentoModel','estabelecimento_id', 'id');
    }

    public function categoria_estabelecimento()
    {
        return $this->belongsTo('App\Models\CategoriaEstabelecimentoModel', 'categoria_estabelecimento_id');
    }

}
