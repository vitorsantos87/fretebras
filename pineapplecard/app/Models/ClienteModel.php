<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
    protected $table = 'clientes';

    public function faturas()
    {
        return $this->hasMany('App\Models\FaturaModel', 'cliente_id', 'id');
    }
}
