<?php

namespace App\Models;

use App\Helpers\Data;
use Illuminate\Database\Eloquent\Model;

class FaturaModel extends Model
{
    protected $table = 'faturas';

    public function lancamentos()
    {
        return $this->hasMany('App\Models\LancamentoModel', 'fatura_id', 'id')->orderBy('data','desc')->orderBy('hora','desc');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\ClienteModel', 'cliente_id');
    }

    public function status_fatura()
    {
        return $this->belongsTo('App\Models\StatusFaturaModel', 'status_fatura_id');
    }



    public static function getSaldoPontosEfetivados($cliente_id){
        return self::where('cliente_id', $cliente_id)
                    ->where('status_fatura_id', StatusFaturaModel::PAGO)
                    ->sum('pontos');

    }

    public static function getSaldoPontosBloqueados($cliente_id){
        return self::where('cliente_id', $cliente_id)
                    ->where('status_fatura_id', '<>' ,StatusFaturaModel::PAGO)
                    ->sum('pontos');

    }

    public static function getTotalGastosEmAberto($cliente_id){
        return self::where('cliente_id', $cliente_id)
                    ->where('status_fatura_id', '<>' ,StatusFaturaModel::PAGO)
                    ->sum('valor_total');
    }

    public static function getUltimaFaturaFechada($cliente_id){
        return self::where('cliente_id', $cliente_id)
                    ->where('data_processamento', '<' ,Data::hoje())
                    ->orderBy('data_vencimento', 'desc')
                    ->first();
    }

    public static function getFaturaAtaulAberta($cliente_id){
        return self::where('cliente_id', $cliente_id)
                    ->where('data_processamento', '>' ,Data::hoje())
                    ->orderBy('data_vencimento', 'asc')
                    ->first();
    }
}
