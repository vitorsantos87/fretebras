<?php

namespace App\Console\Commands;

use App\Services\ArquivoCSV;
use App\Services\Lancamento;
use Illuminate\Console\Command;

class ImportarDados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importar:csv {nome_arquivo}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar arquivo csv para o banco de dados ';


    protected $arquivo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->arquivo = new ArquivoCSV();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $caminho_arquivo = $this->argument('nome_arquivo');

        if(!file_exists ( $caminho_arquivo )){
            $this->error("Erro: Arquivo não encontado");
            return;
        }

        $dados = $this->arquivo
                        ->pulaPrimeiraLinha()
                        ->setCaminho($caminho_arquivo)
                        ->carregarDados();

        if ($dados === FALSE){
            $this->error("Erro: ocorreu um erro ao importar o arquivo!");
        }elseif(count($dados) == 0){
            $this->error("Erro: não foi encontrado nenhum dado dentro do arquivo!");
        }




        $this->info(Lancamento::importar($dados));



    }
}
