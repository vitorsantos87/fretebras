<?php

namespace App\Helpers;

use App\Models\StatusFaturaModel;

class FaturaView {

    public static function getClasseTextoFaturaFechada( $data_vencimento, $status){
        if ($data_vencimento < Data::hoje())
        {
            if ($status == StatusFaturaModel::PAGO)
            {
                return[
                    'classe' => 'success',
                    'texto'  => 'Fatura Paga'
                ];
            }

            return[
                'classe' => 'danger',
                'texto'  => 'Fatura em Atraso'
            ];

        }
        return[
            'classe' => 'danger',
            'texto'  => 'Fatura Fechada'
        ];



    }

}
