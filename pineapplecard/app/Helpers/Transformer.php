<?php
namespace App\Helpers;

class Transformer {

    public static function formatarNumeroDuasCasas($number){
        return number_format($number,  2, ',', '.');
    }

    public static function moneyToView($str){
        return 'R$ '.number_format($str, 2, ',', '.');
    }


}
