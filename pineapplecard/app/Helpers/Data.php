<?php

namespace App\Helpers;

use Carbon\Carbon;

class Data {

    public static function hoje(){
        return date("Y-m-d");
    }

    public static function DataParaView($data){
        return date("d/m/Y", strtotime($data));
    }

    public static function apenasData($data){
        return date("Y-m-d", strtotime($data));
    }

    public static function formarData($dia, $mes, $ano){
        $data = $ano . '-' . $mes . '-' . $dia;
        return date("Y-m-d", strtotime($data));
    }

    public static function apenasDia($data){
        return date("d", strtotime($data));
    }

    public static function apenasMes($data){
        return date("m", strtotime($data));
    }

    public static function apenasAno($data){
        return date("Y", strtotime($data));
    }

    public static function addDias($data , $dias){
        $dt = Carbon::parse($data);
        return $dt->addDays($dias)->toDateTimeString();
    }

    public static function diferencaEmDias($um, $dois){
        $dt1 = Carbon::parse($um);
        $dt2 = Carbon::parse($dois);
        $dias = $dt2->diffInDays($dt1);
        return $dias;

    }

}
