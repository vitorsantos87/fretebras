<?php

namespace App\Services;

use App\Models\ClienteModel;
use App\Models\FaturaModel;
use App\Models\LancamentoModel;

class DadosCliente
{
    public static function getDadosParaApi (ClienteModel $cliente)
    {
        $ultima_fatura_fechada = FaturaModel::getUltimaFaturaFechada($cliente->id);

        $fatura_atual = FaturaModel::getFaturaAtaulAberta($cliente->id);

        if(!$ultima_fatura_fechada || !$fatura_atual){
            throw new \Exception("A fatura do cliente não foi localizada");
        }

        $limite = $cliente->limite - FaturaModel::getTotalGastosEmAberto($cliente->id);

        $saldo_pontos = FaturaModel::getSaldoPontosEfetivados($cliente->id);

        return [
            'ultima_fatura_fechada' => [
                'fatura' => $ultima_fatura_fechada,
                'lancamentos' => LancamentoModel::getLancamentosPorfatura($ultima_fatura_fechada->id)
            ],
            'fatura_atual' => [
                'fatura' => $fatura_atual,
                'lancamentos' => LancamentoModel::getLancamentosPorfatura($fatura_atual->id)
            ],
            'limite' => $limite,
            'saldo_pontos' => $saldo_pontos
        ];
    }

}
