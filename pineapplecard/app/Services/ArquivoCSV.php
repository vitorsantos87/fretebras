<?php

namespace App\Services;

use App\Services\Interfaces\Arquivo;
use Log;

class ArquivoCSV implements Arquivo
{
    protected $caminho = "";
    protected $handle;
    protected $pulaPrimeiraLinha = false;

    public function setCaminho(string $caminho){
        $this->caminho = $caminho;
        return $this;
    }

    public function pulaPrimeiraLinha(){
        $this->pulaPrimeiraLinha = true;
        return $this;
    }

    public function carregarDados(){

        if (!$this->carregaArquivo()){

            return false;

        }
        try {
            $ponteiro = 1;
            $dados_retorno = [];
            while  ($linha  = fgetcsv($this->handle, 0, ";"))  {

                if ($ponteiro == 1 && $this->pulaPrimeiraLinha){
                    $ponteiro++;
                    continue;
                }

                $linha_obj = $this->tratarLinha($linha);

                if(!$linha_obj){
                    return false;
                }

                $dados_retorno[] = $linha_obj;

                $ponteiro++;

            }

            return $dados_retorno;

        } catch (\Exception $e) {
            Log::error('Aconteceu um erro ao ler o arquivo: '. $e->getMessage());
            return false;
        }

    }

    private function carregaArquivo(){

        try {
            $this->handle = fopen($this->caminho, "r");
            return true;
        } catch (\Exception $e) {
            Log::error('Aconteceu um erro ao ler o arquivo: '. $e->getMessage());
            return false;
        }

    }

    private function tratarLinha($linha)
    {
        if (count($linha) != 9 ){
            return false;
        }

        try {
            $obj_linha = (object) [
            'id_lancamento' => $linha[0],
            'data_lancamento' => $linha[1],
            'hora_lancamento' => $linha[2],
            'estabelecimento' => $linha[3],
            'categoria' => $linha[4],
            'latitude' => $linha[5],
            'longitude' => $linha[6],
            'taxa_pontuacao' => $linha[7],
            'valor' => $linha[8] ];

            return $obj_linha;

        } catch (\Exception $e) {
            Log::error('Aconteceu um erro ao ler o arquivo: '. $e->getMessage());
            return false;
        }

    }
}
