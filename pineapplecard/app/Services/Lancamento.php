<?php

namespace App\Services;

use App\Models\LancamentoModel;
use Log;
use DB;

class Lancamento
{
    public static function importar($dados)
    {
        DB::beginTransaction();

        try {
            //$cliente_id, $data_lacamento, $valor, $taxa_pontuacao
            // o cliente está fixo, pois os dados não tem nada sobre o cliente
            $cliente_id = 1;
            foreach ($dados as $lancamento)
            {
                $fatura = Fatura::getFatura($cliente_id, $lancamento->data_lancamento, $lancamento->valor, $lancamento->taxa_pontuacao);

                $estorno = $lancamento->valor < 0 ? true : false;

                $estabelecimento = Estabelecimento::getEstabelecimento($lancamento->estabelecimento,
                                                                        $lancamento->categoria,
                                                                        $lancamento->latitude,
                                                                        $lancamento->longitude,
                                                                        $estorno);

                $inserido = self::InserirMovimentacao($lancamento->id_lancamento,
                                                    $lancamento->valor,
                                                    $lancamento->data_lancamento,
                                                    $lancamento->hora_lancamento,
                                                    $fatura->id,
                                                    $estabelecimento->id,
                                                    $estorno);

                if (!$fatura || !$estabelecimento || !$inserido){
                    DB::rollBack();
                    return "ocorreu um erro ao inportar os dados do arquinvo";
                }


            }

            DB::commit();
            return "Os dados foram importados com sucesso!";

        } catch (\Exception $e) {

            DB::rollBack();

            Log::error('Aconteceu um erro ao importar os dados: '. $e->getMessage());

            return "ocorreu um erro ao inportar os dados do arquivo";
        }
    }

    private static function InserirMovimentacao($id_lancamento, $valor, $data, $hora, $fatura_id, $estabelecimento_id, $estorno)
    {
        $lancamento = LancamentoModel::find($id_lancamento);

        if (!$lancamento)
        {
            $lancamento = new LancamentoModel();
            $lancamento->id = $id_lancamento;
            $lancamento->valor = $valor;
            $lancamento->data = $data;
            $lancamento->hora = $hora;
            $lancamento->fatura_id = $fatura_id;
            $lancamento->estabelecimento_id = $estabelecimento_id;
            $lancamento->estorno = $estorno;
            $lancamento->save();

        }

        return true;

    }



}
