<?php

namespace App\Services\Interfaces;

Interface Arquivo
{
    public function setCaminho(string $caminho);
    public function carregarDados();
}
