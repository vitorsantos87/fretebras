<?php

namespace App\Services;

use App\Helpers\Data;
use App\Models\ClienteModel;
use App\Models\FaturaModel;
use App\Models\StatusFaturaModel;
use Log;
use DB;

class Fatura
{


    public static function getFatura($cliente_id, $data_lacamento, $valor, $taxa_pontuacao )
    {
        try {
            $cliente = ClienteModel::find($cliente_id);

            if (!$cliente){
                return false;
            }


            $data_fatura = self::getMesAnoFatura($data_lacamento,$cliente->dia_pagamento);
            $data_vencimento = Data::formarData($cliente->dia_pagamento, $data_fatura['mes'], $data_fatura['ano']);
            $data_fechamento = Data::formarData(self::getDiaFechamento($cliente->dia_pagamento), $data_fatura['mes'], $data_fatura['ano']);

            $fatura = FaturaModel::where('cliente_id', $cliente_id)
                                ->where('mes_referencia', $data_fatura['mes'])
                                ->where('ano_referencia', $data_fatura['ano'])
                                ->first();

            if (!$fatura){
                $fatura = new FaturaModel();
                $fatura->cliente_id = $cliente_id;
                $fatura->mes_referencia = $data_fatura['mes'];
                $fatura->ano_referencia = $data_fatura['ano'];
                $fatura->data_vencimento = $data_vencimento;
                $fatura->data_processamento = $data_fechamento;
            }

            $fatura->status_fatura_id = Data::hoje() > $data_fechamento ? StatusFaturaModel::FECHADA : StatusFaturaModel::ABERTA;
            $fatura->valor_total = empty($fatura->valor_total) ? $valor : $fatura->valor_total + $valor;
            $pontos = $valor * $taxa_pontuacao;
            $fatura->pontos = empty($fatura->pontos) ? $pontos : $fatura->pontos + $pontos;

            $fatura->save();

            return $fatura;

        } catch (\Exception $e) {

            Log::error('Aconteceu um erro ao retornar a fatura: '. $e->getMessage());
            return false;
        }


    }


    // dá para melhorar esse algoritmo
    private static function getMesAnoFatura($data_lacamento, $dia_pagamento){
        $dia_fechamento = self::getDiaFechamento($dia_pagamento);

        $mes_fatura = Data::apenasMes($data_lacamento);
        $ano_fatura = Data::apenasAno($data_lacamento);
        $dia_lancamento = Data::apenasDia($data_lacamento);

        if ($dia_pagamento > $dia_fechamento){

            if ($dia_lancamento <= $dia_fechamento)
            {
                return [
                    'mes' => $mes_fatura,
                    'ano' => $ano_fatura
                ];
            }

            return [
                'mes' => $mes_fatura < 12 ? $mes_fatura + 1 : 1,
                'ano' => $mes_fatura < 12 ? $ano_fatura : $ano_fatura + 1
            ];

        }

        //soma 1 ou 2
        if ($dia_lancamento <= $dia_fechamento)
        {
            return [
                'mes' => $mes_fatura < 12 ? $mes_fatura + 1 : 1,
                'ano' => $mes_fatura < 12 ? $ano_fatura : $ano_fatura + 1
            ];
        }
        $resto = (($mes_fatura + 2) % 12);
        $mes_fatura = $resto == 0 ? 12 : $resto;
        $ano_fatura = $resto <= 2 ? $ano_fatura + 1 : $ano_fatura;

        return [
            'mes' => $mes_fatura,
            'ano' => $ano_fatura
        ];

    }

    private static function getDiaFechamento($dia_pagamento){
        return ($dia_pagamento <= 10) ? (30 - ($dia_pagamento - 10)) : ($dia_pagamento - 10);
    }

}
