<?php

namespace App\Services;

use App\Models\CategoriaEstabelecimentoModel;
use App\Models\EstabelecimentoModel;
use Log;
use DB;

class Estabelecimento
{

    public static function getEstabelecimento($nome, $categoria, $latitude, $longitude, $estorno = false){

        try {
            if ($estorno){
                $arr_nome = explode('/', $nome);
                $nome = $arr_nome[1];
            }

            $estabelecimento = EstabelecimentoModel::where('nome', trim($nome))->first();
            $categoria =  CategoriaEstabelecimentoModel::find($categoria);

            if (!$estabelecimento){
                $estabelecimento = new EstabelecimentoModel();
                $estabelecimento->nome = trim($nome);
                $estabelecimento->latitude = $latitude;
                $estabelecimento->longitude = $longitude;
                $estabelecimento->categoria_estabelecimento_id = $categoria->id;
                $estabelecimento->save();
            }
            return $estabelecimento;

        } catch (\Exception $e) {
            Log::error('Aconteceu um erro ao pegar a categoria: '. $e->getMessage());

            return false;
        }
    }

}
