<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ClienteModel;
use App\Services\DadosCliente;
use Log;

class AcompanhamentoController extends Controller
{
    public function get($cliente_id)
    {

        try {

            $cliente = ClienteModel::find($cliente_id);

            if(!$cliente){
                return response()->json([
                    'status' => false,
                    'message' => "Cliente não encontrado!"]
                    , 404);
            }

            return response()->json([
                'status' => true,
                'dados' => DadosCliente::getDadosParaApi($cliente)]
                , 200);


        } catch (\Exception $e ){
            Log::error('Aconteceu um erro buscar os dados do cliente: '. $e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()]
                , 412);
        }



    }
}
