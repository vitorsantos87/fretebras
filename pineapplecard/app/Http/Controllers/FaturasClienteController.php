<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;


class FaturasClienteController extends Controller
{
    public function getDados(){

        try {

            // está fixo pois nos dados enviados não tem nada relacionado ao cliente
            $cliente_id = 1;

            $url = env('APP_URL').'/api/v1/acompanhamento/'. $cliente_id;

            $cliente = new \GuzzleHttp\Client();
            $result = $cliente->post($url, [
                'form_params' => []
            ]);

            $statusCode = $result->getStatusCode();
            $body = json_decode($result->getBody());
            return view('fatura', ['retorno' => $body]);

        }catch (\GuzzleHttp\Exception\ClientException $th) {
            $message = json_decode($th->getResponse()->getBody()->getContents());
            Log::debug('Consulta Fatura do cliente -'. json_encode($message));
            return view('erro', ['retorno' => $message]);
        }

    }
}
