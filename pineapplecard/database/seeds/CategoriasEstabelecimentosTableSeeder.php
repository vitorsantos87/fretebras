<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CategoriasEstabelecimentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 0.8
        ]);


        //2
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 1
        ]);


        //3
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 1.4
        ]);

        //4
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 1.8
        ]);

        //5
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 2.3
        ]);

        //6
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 0.3
        ]);


        //7
        DB::table('categorias_estabelecimentos')->insert([
            'nome' => Str::random(10),
            'taxa_pontuacao' => 2.7
        ]);
    }
}
