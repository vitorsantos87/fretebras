<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusFaturasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('status_faturas')->insert([
            'nome' => "Aberta"
        ]);

        //2
        DB::table('status_faturas')->insert([
            'nome' => "Fechada"
        ]);

        //3
        DB::table('status_faturas')->insert([
            'nome' => "Paga"
        ]);
    }
}
