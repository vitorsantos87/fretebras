<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cliente_id')->unsigned();
            $table->smallInteger('mes_referencia');
            $table->year('ano_referencia');
            $table->date('data_vencimento');
            $table->date('data_processamento');
            $table->date('data_pagamento')->nullable();
            $table->double('valor_total', 10, 2);
            $table->double('valor_pago', 10, 2)->nullable();
            $table->bigInteger('status_fatura_id')->unsigned();
            $table->bigInteger('pontos');
            $table->timestamps();

            $table->index(['cliente_id', 'mes_referencia', 'ano_referencia']);

            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('status_fatura_id')->references('id')->on('status_faturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faturas');
    }
}
