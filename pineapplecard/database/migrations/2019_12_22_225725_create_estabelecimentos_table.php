<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstabelecimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('categoria_estabelecimento_id')->unsigned();
            $table->string('nome');
            $table->boolean('status')->default(true);
            $table->double('latitude', 15, 10);
            $table->double('longitude', 15, 10);
            $table->timestamps();

            $table->foreign('categoria_estabelecimento_id')->references('id')->on('categorias_estabelecimentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estabelecimentos');
    }
}
