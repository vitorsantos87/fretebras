<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FaturaClienteTest extends TestCase
{

    public function testClienteExiste()
    {
        $prefix = env('APP_URL')."/api/v1";

        $cliente_id = "1";
        $response = $this->post("$prefix/acompanhamento/$cliente_id");
        $response->assertStatus(200);
        // o json é muito grande e depende muito das modificações do banco
    }


    public function testClienteNaoExiste()
    {
        $prefix = env('APP_URL')."/api/v1";

        $cliente_id = "2";
        $response = $this->post("$prefix/acompanhamento/$cliente_id");
        $response->assertStatus(404);
        $response->assertExactJson(
            [
                'status' => false,
                'message' => "Cliente não encontrado!"
            ]
        );
    }

    public function testSemClienteExiste()
    {
        $prefix = env('APP_URL')."/api/v1";

        $response = $this->post("$prefix/acompanhamento");
        $response->assertStatus(404);
    }

}
