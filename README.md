# fretebras
- Foi utilizada a linguagem PHP na versão 7.3 e o framework Laravel na versão 6.2  

- São 3 dockers: um para o php, outro para o projeto web com o servidor de aplicações nginx. Além de um terceiro com o Mysql. Os dockers estão junto com o projeto laravel.

- Para rodar os dockers só rodar na raiz do projeto laravel: docker-compose up -d

- Para parar os dockers: docker-compose down

- A criação das tabelas do banco de dados pode ser feita via migrations. Os dados  de status e categorias estão como seed.

- Como rodar comandos artisan na estrutura de dockers: docker-compose exec php  php  artisan [comando]

- Rodar comando do composer: docker-compose exec php composer [comando]

- Para importar o arquivo csv basta roda o comando (se for rodar no docker só inserir "importar:csv [caminho]" no comando acima ) :  php artisan importar:csv [caminho] 

- Para rodar os testes no docker: docker-compose exec php vendor/bin/phpunit


# Seeders:

    -  docker-compose exec php  php  artisan db:seed --class=ClientesTableSeeder
    -  docker-compose exec php  php  artisan db:seed --class=CategoriasEstabelecimentosTableSeeder
    -  docker-compose exec php  php  artisan db:seed --class=StatusFaturasTableSeeder

# Observações:

- Como no CSV não tem nada especificando nada em relação a cliente, estou assumindo que todos os lançamentos são de um cliente fictício com o id 1.

- Os erros poderiam ser mais específicos, poderia ter uma exception específica para cada tipo de erro. Por causa da falta de tempo vou utilizar erros simples para que a entrega seja feita a tempo.

- Vou considerar que o mês tem 30 dias para que possa ficar mais simples determinar a data do fechamento da fatura

- Vou considerar para este projeto que o mês referência da fatura corresponde ao mês de vencimento da fatura e não do lançamento.

- Como não temos uma taxa de juros, não vou considerar isso na hora de estabelecer o valor da fatura. E apesar da modelagem contemplar, vou considerar que o pagamento e sempre total. Caso contrario deveriam existir rotinas complexas para os cálculos.

- A parte da página web foi utilizado o mesmo projeto laravel, mas a chamada realizada via API.

- Foi utilizado apenas um endpoint de API. Pela simplicidade da tela não viu-se necessidade de separar os componentes e então fazer vários endpoints. Poderia ser uma boa prática separar para futuros usos da API, porém neste caso, acredito que seja mais conveniente refatorar a API, já que não teremos esse uso de imediato.

- O status da fatura deveria ser controlado por rotinas internas, mas para o teste vou mostrar pela data de hoje.

- Os testes realizados são de feature